#include "Martin_Vedris_header_Usmeni.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

void kreiranje_Datoteke(void)
{
	//Ovu funkcija je korisna samo prilikom prvog pokretanja programa. Kasnije nam nije potrebna jer datoteka vec postoji, ali ju ipak ostavljamo unutar programa. Program prilikom pokretanja ne pokazuje poruke greske jer se prebrzo vrti pa tako ne dobijemo poruke da datoteka ne postoji na disku vec samo glavni izbornik. Prilikom testiranja sam koristio _getch(); da bi se uvjerio da se ova funkcija stvarno izvrsi

	FILE* pokNaDat = NULL;
	pokNaDat = fopen("datoteka.bin", "rb");

	if (pokNaDat == NULL) {
		perror("Datoteka ne postoji prilikom prvog pokretanja");
		//_getch();

		pokNaDat = fopen("datoteka.bin", "wb");

		if (pokNaDat == NULL) {
			perror("Datoteka se ne moze kreirati");
			exit(EXIT_FAILURE);
		}
		else {
			fclose(pokNaDat);
			printf("Datoteka kreirana prilikom prvog pokretanja");
			//_getch();
		}
	}
	else {
		fclose(pokNaDat);
		//printf("Datoteka kreirana prilikom prvog pokretanja");
		//_getch();
	}
}

void izbornik(void)
{
	int izbornik = -1;

	while (1) {
		system("cls");

		int brojac = brojac_Voca();
		printf("\tBroj voca u datoteka.bin: %d", brojac);

		printf("\n-----------------------------------");
		printf("\n1. Unos podataka");
		printf("\n2. Citanje podataka");
		printf("\n3. Ispis cijelog skladista");
		printf("\n4. Sortiranje podataka");
		printf("\n5. Brisanje podataka");
		printf("\n6. Zavrsetak programa");
		printf("\n-----------------------------------");
		printf("\nUnesite broj operacije koji zelite izvrsiti: ");
		scanf("%d", &izbornik);

		switch (izbornik)
		{
			case 1:
				unos_Podataka();
				break;
			case 2:
				citanje_Izbornik();
				break;
			case 3:
				ispis_Cijelog_Skladista();
				break;
			case 4:
				sortiranje_Izbornik();
				break;
			case 5:
				brisanje_Podataka_Izbornik();
				break;
			case 6:
				zavrsetak_Programa();
				break;
		}
	}
}

int brojac_Voca(void)
{
	FILE* pokNaDat = NULL;
	pokNaDat = fopen("datoteka.bin", "rb");

	if (pokNaDat == NULL) {
		perror("Otvaranje");
		return 1;
	}
	else {
		fseek(pokNaDat, 0, SEEK_END);		
		int brojac = ftell(pokNaDat) / sizeof(VOCARNA);
		fclose(pokNaDat);
		return brojac;
	}
}

void unos_Podataka(void)
{
	FILE* pokNaDat = NULL;
	pokNaDat = fopen("datoteka.bin", "ab");

	if (pokNaDat == NULL) {
		perror("Izbornik 1 - Unos podataka");
		return;
		//exit(EXIT_FAILURE);
	}
	else {

		int brojac = brojac_Voca();
		printf("\n\tBroj voca u datoteka.bin: %d\n\n", brojac);

		VOCARNA* unosVoca = NULL;
		unosVoca = (VOCARNA*)calloc(1, sizeof(VOCARNA));
		if (unosVoca == NULL) {
			perror("Greska pri dinamickom zauzimanju");
			exit(EXIT_FAILURE);
		}

		printf("Unesite ID voca: ");
		scanf("%d", &unosVoca->ID);
		printf("Unesite ime voca: ");
		scanf(" %20[^\n]", unosVoca->ime);
		printf("Unesite cijenu voca: ");
		scanf("%f", &unosVoca->cijena);
		printf("Unesite kolicinu voca: ");
		scanf("%d", &unosVoca->kolicina);
	
		fwrite(unosVoca, sizeof(VOCARNA), 1, pokNaDat);
		fclose(pokNaDat);

		printf("\nPritisnite ENTER za nastavak: ");
		_getch();
		return;
	}
}

void citanje_Izbornik(void)
{
	int citanje = 0;
	int brojac = brojac_Voca();

	do {
		system("cls");

		printf("\tBroj voca u datoteka.bin: %d", brojac);

		printf("\n-----------------------------------");
		printf("\n1. ID");
		printf("\n2. Ime");
		printf("\n3. Cijena");
		printf("\n4. Kolicina");
		printf("\n-----------------------------------");
		printf("\nUnesite broj parametra po kojem zelite pretraziti skladiste: ");
		scanf("%d", &citanje);
	} while (citanje < 1 || citanje > 4);

	switch (citanje)
	{
		case 1:
			citanjeID();
			break;
		case 2:
			citanjeIme();
			break;
		case 3:
			citanjeCijena();
			break;
		case 4:
			citanjeKolicina();
			break;
	}
	
	printf("\nPritisnite ENTER za nastavak: ");
	_getch();
	return;
}

void citanjeID(void)
{
	FILE* pokNaDat = NULL;
	pokNaDat = fopen("datoteka.bin", "rb");

	if (pokNaDat == NULL) {
		perror("Izbornik 2 - Citanje podataka, ID");
		return;
		//exit(EXIT_FAILURE);
	}
	else {
		VOCARNA* localVocarna = NULL;
		int brojac = brojac_Voca();

		localVocarna = (VOCARNA*)calloc(brojac, sizeof(VOCARNA));
		if (localVocarna == NULL) {
			perror("Greska pri dinamickom zauzimanju!");
			exit(EXIT_FAILURE);
		}

		int trazeniID, osigurac = 0;
		printf("\nUnesite ID koji zelite pretraziti: ");
		scanf("%d", &trazeniID);

		for (int i = 0; i < brojac; i++) {
			fread(localVocarna, sizeof(VOCARNA), 1, pokNaDat);

			if (localVocarna->ID == trazeniID) {
				fprintf(stdout, "--------------------\n\n");
				fprintf(stdout, "ID: %d\n", localVocarna->ID);
				fprintf(stdout, "Ime: %s\n", localVocarna->ime);
				fprintf(stdout, "Cijena: %.2f\n", localVocarna->cijena);
				fprintf(stdout, "Kolicina: %d\n\n", localVocarna->kolicina);
				fprintf(stdout, "--------------------\n");
				osigurac = 1;
			}
		}
		if (osigurac == 0) {
			printf("\nNe postoji voce s trazenim ID-om!\n");
		}
		
		fclose(pokNaDat);
		free(localVocarna);
	}
}

void citanjeIme(void)
{
	FILE* pokNaDat = NULL;
	pokNaDat = fopen("datoteka.bin", "rb");

	if (pokNaDat == NULL) {
		perror("Izbornik 2 - Citanje podataka, Ime");
		return;
		//exit(EXIT_FAILURE);
	}
	else {
		VOCARNA* localVocarna = NULL;
		int brojac = brojac_Voca();

		localVocarna = (VOCARNA*)calloc(brojac, sizeof(VOCARNA));
		if (localVocarna == NULL) {
			perror("Greska pri dinamickom zauzimanju");
			exit(EXIT_FAILURE);
		}

		int osigurac = 0;
		char trazenoIme[20];
		printf("\nUnesite ime koje zelite pretraziti: ");
		scanf(" %20[^\n]", trazenoIme);

		for (int i = 0; i < brojac; i++) {
			fread(localVocarna, sizeof(VOCARNA), 1, pokNaDat);

			if (strcmp(localVocarna->ime, (trazenoIme)) == 0) {
				fprintf(stdout, "--------------------\n\n");
				fprintf(stdout, "ID: %d\n", localVocarna->ID);
				fprintf(stdout, "Ime: %s\n", localVocarna->ime);
				fprintf(stdout, "Cijena: %.2f\n", localVocarna->cijena);
				fprintf(stdout, "Kolicina: %d\n\n", localVocarna->kolicina);
				fprintf(stdout, "--------------------\n");
				osigurac = 1;
			}
		}
		if (osigurac == 0) {
			printf("\nNe postoji voce s trazenim imenom!\n");
		}

		fclose(pokNaDat);
		free(localVocarna);
	}
}

void citanjeCijena(void)
{
	FILE* pokNaDat = NULL;
	pokNaDat = fopen("datoteka.bin", "rb");

	if (pokNaDat == NULL) {
		perror("Izbornik 2 - Citanje podataka, Cijena");
		return;
		//exit(EXIT_FAILURE);
	}
	else {
		VOCARNA* localVocarna = NULL;
		int brojac = brojac_Voca();

		localVocarna = (VOCARNA*)calloc(brojac, sizeof(VOCARNA));
		if (localVocarna == NULL) {
			perror("Greska pri dinamickom zauzimanju!");
			exit(EXIT_FAILURE);
		}

		float trazenaCijena;
		int osigurac = 0;
		printf("\nUnesite cijenu koju zelite pretraziti: ");
		scanf("%f", &trazenaCijena);

		for (int i = 0; i < brojac; i++) {
			fread(localVocarna, sizeof(VOCARNA), 1, pokNaDat);

			if (localVocarna->cijena == trazenaCijena) {
				fprintf(stdout, "--------------------\n\n");
				fprintf(stdout, "ID: %d\n", localVocarna->ID);
				fprintf(stdout, "Ime: %s\n", localVocarna->ime);
				fprintf(stdout, "Cijena: %.2f\n", localVocarna->cijena);
				fprintf(stdout, "Kolicina: %d\n\n", localVocarna->kolicina);
				fprintf(stdout, "--------------------\n");
				osigurac = 1;
			}
		}
		if (osigurac == 0) {
			printf("\nNe postoji voce s trazenom cijenom!\n");
		}

		fclose(pokNaDat);
		free(localVocarna);
	}
}

void citanjeKolicina(void)
{
	FILE* pokNaDat = NULL;
	pokNaDat = fopen("datoteka.bin", "rb");

	if (pokNaDat == NULL) {
		perror("Izbornik 2 - Citanje podataka, Kolicina");
		return;
		//exit(EXIT_FAILURE);
	}
	else {
		VOCARNA* localVocarna = NULL;
		int brojac = brojac_Voca();

		localVocarna = (VOCARNA*)calloc(brojac, sizeof(VOCARNA));
		if (localVocarna == NULL) {
			perror("Greska pri dinamickom zauzimanju!");
			exit(EXIT_FAILURE);
		}

		int trazenaKolicina, osigurac = 0;
		printf("\nUnesite kolicinu koju zelite pretraziti: ");
		scanf("%d", &trazenaKolicina);

		for (int i = 0; i < brojac; i++) {
			fread(localVocarna, sizeof(VOCARNA), 1, pokNaDat);

			if (localVocarna->kolicina == trazenaKolicina) {
				fprintf(stdout, "--------------------\n\n");
				fprintf(stdout, "ID: %d\n", localVocarna->ID);
				fprintf(stdout, "Ime: %s\n", localVocarna->ime);
				fprintf(stdout, "Cijena: %.2f\n", localVocarna->cijena);
				fprintf(stdout, "Kolicina: %d\n\n", localVocarna->kolicina);
				fprintf(stdout, "--------------------\n");
				osigurac = 1;
			}
		}
		if (osigurac == 0) {
			printf("\nNe postoji voce s trazenom kolicinom!\n");
		}

		fclose(pokNaDat);
		free(localVocarna);
	}
}

void ispis_Cijelog_Skladista(void)
{
	FILE* pokNaDat = NULL;
	pokNaDat = fopen("datoteka.bin", "rb");

	if (pokNaDat == NULL) {
		perror("Izbornik 3 - Ispis cijelog skladista");
		return;
		//exit(EXIT_FAILURE);
	}
	else {
		VOCARNA* localVocarna = NULL;
		int brojac = brojac_Voca();

		localVocarna = (VOCARNA*)calloc(brojac, sizeof(VOCARNA));
		if (localVocarna == NULL) {
			perror("Greska pri dinamickom zauzimanju");
			exit(EXIT_FAILURE);
		}

		printf("\nSVA VOCA UNUTAR DATOTEKE\n");
		for (int i = 0; i < brojac; i++) {
			fread(localVocarna, sizeof(VOCARNA), 1, pokNaDat);
			fprintf(stdout, "--------------------\n\n");
			fprintf(stdout, "ID: %d\n", localVocarna->ID);
			fprintf(stdout, "Ime: %s\n", localVocarna->ime);
			fprintf(stdout, "Cijena: %.2f\n", localVocarna->cijena);
			fprintf(stdout, "Kolicina: %d\n\n", localVocarna->kolicina);
			fprintf(stdout, "--------------------\n");
		}

		fclose(pokNaDat);
		free(localVocarna);

		printf("\nPritisnite ENTER za nastavak: ");
		_getch();
		return;
	}
}

void sortiranje_Izbornik(void)
{
	int sortiranje = 0;
	int brojac = brojac_Voca();

	do {
		system("cls");

		printf("\tBroj voca u datoteka.bin: %d", brojac);

		printf("\n-----------------------------------");
		printf("\n1. Quick sort ID - Uzlazno");
		printf("\n2. Quick sort ID - Silazno");
		printf("\n-----------------------------------");
		printf("\nUnesite broj operacije po kojoj zelite sortirat skladiste: ");
		scanf("%d", &sortiranje);
	} while ((sortiranje != 1) && (sortiranje != 2));

	switch (sortiranje)
	{
		case 1:
			uzl_Sort_ID();
			break;
		case 2:
			sil_Sort_ID();
			break;
	}

	printf("\nPritisnite ENTER za nastavak: ");
	_getch();
	return;
}

void uzl_Sort_ID(void)
{
	FILE* pokNaDat = NULL;
	pokNaDat = fopen("datoteka.bin", "rb");

	if (pokNaDat == NULL) {
		perror("Izbornik 4 - Quick sort ID - Uzlazno");
		return;
		//exit(EXIT_FAILURE);
	}
	else {
		VOCARNA* localVocarna = NULL;
		int brojac = brojac_Voca();

		localVocarna = (VOCARNA*)calloc(brojac, sizeof(VOCARNA));
		if (localVocarna == NULL) {
			perror("Greska pri dinamickom zauzimanju!");
			exit(EXIT_FAILURE);
		}

		fread(localVocarna, sizeof(VOCARNA), brojac, pokNaDat);
		qsort(localVocarna, brojac, sizeof(VOCARNA), uzlaznoSortiranjeID);
		for (int i = 0; i < brojac; i++) {
			fprintf(stdout, "--------------------\n\n");
			fprintf(stdout, "ID: %d\n", (localVocarna + i)->ID);
			fprintf(stdout, "Ime: %s\n", (localVocarna + i)->ime);
			fprintf(stdout, "Cijena: %.2f\n", (localVocarna + i)->cijena);
			fprintf(stdout, "Kolicina: %d\n\n", (localVocarna + i)->kolicina);
			fprintf(stdout, "--------------------\n");
		}
	}
}

void sil_Sort_ID(void)
{
	FILE* pokNaDat = NULL;
	pokNaDat = fopen("datoteka.bin", "rb");

	if (pokNaDat == NULL) {
		perror("Izbornik 4 - Quick sort ID - Silazno");
		return;
		//exit(EXIT_FAILURE);
	}
	else {
		VOCARNA* localVocarna = NULL;
		int brojac = brojac_Voca();

		localVocarna = (VOCARNA*)calloc(brojac, sizeof(VOCARNA));
		if (localVocarna == NULL) {
			perror("Greska pri dinamickom zauzimanju!");
			exit(EXIT_FAILURE);
		}

		fread(localVocarna, sizeof(VOCARNA), brojac, pokNaDat);
		qsort(localVocarna, brojac, sizeof(VOCARNA), silaznoSortiranjeID);
		for (int i = 0; i < brojac; i++) {
			fprintf(stdout, "--------------------\n\n");
			fprintf(stdout, "ID: %d\n", (localVocarna + i)->ID);
			fprintf(stdout, "Ime: %s\n", (localVocarna + i)->ime);
			fprintf(stdout, "Cijena: %.2f\n", (localVocarna + i)->cijena);
			fprintf(stdout, "Kolicina: %d\n\n", (localVocarna + i)->kolicina);
			fprintf(stdout, "--------------------\n");
		}
	}
}

int uzlaznoSortiranjeID(const void* x, const void* y) 
{
	VOCARNA* X = (VOCARNA*)x;
	VOCARNA* Y = (VOCARNA*)y;

	return (X->ID - Y->ID);
}

int silaznoSortiranjeID(const void* x, const void* y) 
{
	VOCARNA* X = (VOCARNA*)x;
	VOCARNA* Y = (VOCARNA*)y;

	return (Y->ID - X->ID);
}

void brisanje_Podataka_Izbornik(void)
{
	int brisanje = 0;
	int brojac = brojac_Voca();

	do {
		system("cls");

		printf("\tBroj voca u datoteka.bin: %d", brojac);

		printf("\n-----------------------------------");
		printf("\n1. ID");
		printf("\n2. Ime");
		printf("\n3. Cijena");
		printf("\n4. Kolicina");
		printf("\n-----------------------------------");
		printf("\nUnesite broj parametra po kojem zelite obrisati voce iz skladista: ");
		scanf("%d", &brisanje);
	} while (brisanje < 1 || brisanje > 4);

	switch (brisanje)
	{
	case 1:
		brisanjeID();
		break;
	case 2:
		brisanjeIme();
		break;
	case 3:
		brisanjeCijena();
		break;
	case 4:
		brisanjeKolicina();
		break;
	}

	printf("\nPritisnite ENTER za nastavak: ");
	_getch();
	return;
}

void brisanjeID(void)
{
	FILE* pokNaDat = NULL;
	pokNaDat = fopen("datoteka.bin", "rb");

	if (pokNaDat == NULL) {
		perror("Izbornik 5 - Brisanje podataka, ID");
		return;
		//exit(EXIT_FAILURE);
	}
	else {
		FILE* tempPokNaDat = NULL;
		tempPokNaDat = fopen("temp.bin", "wb");

		if (tempPokNaDat == NULL) {
			perror("Izbornik 5 - Brisanje podataka, ID");
			return;
			//exit(EXIT_FAILURE);
		}
		else {
			VOCARNA* localVocarna = NULL;
			int brojac = brojac_Voca();

			localVocarna = (VOCARNA*)calloc(brojac, sizeof(VOCARNA));
			if (localVocarna == NULL) {
				perror("Greska pri dinamickom zauzimanju");
				exit(EXIT_FAILURE);
			}

			int brisanje_ID, found = 0;
			printf("\nUnesite ID koji zelite obrisati: ");
			scanf("%d", &brisanje_ID);

			for (int i = 0; i < brojac; i++) {
				fread(localVocarna, sizeof(VOCARNA), 1, pokNaDat);
				if (localVocarna->ID == brisanje_ID) {
					printf("\nVoce s trazenim ID-om nadeno i obrisano\n");
					found = 1;
				}
				else {
					fwrite(localVocarna, sizeof(VOCARNA), 1, tempPokNaDat);
				}
			}
			if (found == 0) {
				printf("\nNe postoji voce s trazenim ID-om\n");
			}
		}
		fclose(pokNaDat);
		fclose(tempPokNaDat);

		remove("datoteka.bin");
		rename("temp.bin", "datoteka.bin");
	}
}

void brisanjeIme(void)
{
	FILE* pokNaDat = NULL;
	pokNaDat = fopen("datoteka.bin", "rb");

	if (pokNaDat == NULL) {
		perror("Izbornik 5 - Brisanje podataka, Ime");
		return;
		//exit(EXIT_FAILURE);
	}
	else {
		FILE* tempPokNaDat = NULL;
		tempPokNaDat = fopen("temp.bin", "wb");

		if (tempPokNaDat == NULL) {
			perror("Izbornik 5 - Brisanje podataka, Ime");
			return;
			//exit(EXIT_FAILURE);
		}
		else {
			VOCARNA* localVocarna = NULL;
			int brojac = brojac_Voca();

			localVocarna = (VOCARNA*)calloc(brojac, sizeof(VOCARNA));
			if (localVocarna == NULL) {
				perror("Greska pri dinamickom zauzimanju");
				exit(EXIT_FAILURE);
			}

			int found = 0;
			char brisanje_Ime[20];
			printf("\nUnesite ime koje zelite obrisati: ");
			scanf(" %20[^\n]", &brisanje_Ime);

			for (int i = 0; i < brojac; i++) {
				fread(localVocarna, sizeof(VOCARNA), 1, pokNaDat);
				if (strcmp(localVocarna->ime, (brisanje_Ime)) == 0) {
					printf("\nVoce s trazenim imenom nadeno i obrisano\n");
					found = 1;
				}
				else {
					fwrite(localVocarna, sizeof(VOCARNA), 1, tempPokNaDat);
				}
			}
			if (found == 0) {
				printf("\nNe postoji voce s trazenim imenom\n");
			}
		}
		fclose(pokNaDat);
		fclose(tempPokNaDat);

		remove("datoteka.bin");
		rename("temp.bin", "datoteka.bin");
	}
}

void brisanjeCijena(void)
{
	FILE* pokNaDat = NULL;
	pokNaDat = fopen("datoteka.bin", "rb");

	if (pokNaDat == NULL) {
		perror("Izbornik 5 - Brisanje podataka, Cijena");
		return;
		//exit(EXIT_FAILURE);
	}
	else {
		FILE* tempPokNaDat = NULL;
		tempPokNaDat = fopen("temp.bin", "wb");

		if (tempPokNaDat == NULL) {
			perror("Izbornik 5 - Brisanje podataka, Cijena");
			return;
			//exit(EXIT_FAILURE);
		}
		else {
			VOCARNA* localVocarna = NULL;
			int brojac = brojac_Voca();

			localVocarna = (VOCARNA*)calloc(brojac, sizeof(VOCARNA));
			if (localVocarna == NULL) {
				perror("Greska pri dinamickom zauzimanju");
				exit(EXIT_FAILURE);
			}

			int found = 0;
			float brisanje_Cijena;
			printf("\nUnesite cijenu koju zelite obrisati: ");
			scanf("%f", &brisanje_Cijena);

			for (int i = 0; i < brojac; i++) {
				fread(localVocarna, sizeof(VOCARNA), 1, pokNaDat);
				if (localVocarna->cijena == brisanje_Cijena) {
					printf("\nVoce s trazenim cijenom nadeno i obrisano\n");
					found = 1;
				}
				else {
					fwrite(localVocarna, sizeof(VOCARNA), 1, tempPokNaDat);
				}
			}
			if (found == 0) {
				printf("\nNe postoji voce s trazenom cijenom\n");
			}
		}
		fclose(pokNaDat);
		fclose(tempPokNaDat);

		remove("datoteka.bin");
		rename("temp.bin", "datoteka.bin");
	}
}

void brisanjeKolicina(void)
{
	FILE* pokNaDat = NULL;
	pokNaDat = fopen("datoteka.bin", "rb");

	if (pokNaDat == NULL) {
		perror("Izbornik 5 - Brisanje podataka, Kolicina");
		return;
		//exit(EXIT_FAILURE);
	}
	else {
		FILE* tempPokNaDat = NULL;
		tempPokNaDat = fopen("temp.bin", "wb");

		if (tempPokNaDat == NULL) {
			perror("Izbornik 5 - Brisanje podataka, Kolicina");
			return;
			//exit(EXIT_FAILURE);
		}
		else {
			VOCARNA* localVocarna = NULL;
			int brojac = brojac_Voca();

			localVocarna = (VOCARNA*)calloc(brojac, sizeof(VOCARNA));
			if (localVocarna == NULL) {
				perror("Greska pri dinamickom zauzimanju");
				exit(EXIT_FAILURE);
			}

			int brisanje_Kolicina, found = 0;
			printf("\nUnesite kolicinu koju zelite obrisati: ");
			scanf("%d", &brisanje_Kolicina);

			for (int i = 0; i < brojac; i++) {
				fread(localVocarna, sizeof(VOCARNA), 1, pokNaDat);
				if (localVocarna->kolicina == brisanje_Kolicina) {
					printf("\nVoce s trazenom kolicinom nadeno i obrisano\n");
					found = 1;
				}
				else {
					fwrite(localVocarna, sizeof(VOCARNA), 1, tempPokNaDat);
				}
			}
			if (found == 0) {
				printf("\nNe postoji voce s trazenom kolicinom\n");
			}
		}
		fclose(pokNaDat);
		fclose(tempPokNaDat);

		remove("datoteka.bin");
		rename("temp.bin", "datoteka.bin");
	}
}

void zavrsetak_Programa(void)
{
	char odluka[3];

	printf("\nJeste li sigurni da zelite izaci iz programa");
	printf("\nUnesite DA/NE: ");
	scanf(" %2s", &odluka);

	if (strcmp("DA", odluka) == 0) {
		exit(EXIT_SUCCESS);
	}
	return;
}