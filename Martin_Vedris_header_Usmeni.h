#ifndef MARTIN_VEDRIS_HEADER_USMENI
#define MARTIN_VEDRIS_HEADER_USMENI

#define _CRT_SECURE_NO_WARNINGS

typedef struct vocarna {
	int ID;
	char ime[20];
	float cijena;
	int kolicina;
}VOCARNA;

void kreiranje_Datoteke(void);

//glavni izbornik
void izbornik(void);

int brojac_Voca(void);
void unos_Podataka(void);

//citanje
void citanje_Izbornik(void);
void citanjeID(void);
void citanjeIme(void);
void citanjeCijena(void);
void citanjeKolicina(void);

void ispis_Cijelog_Skladista(void);

//sortiranje
void sortiranje_Izbornik(void);
void uzl_Sort_ID(void);
void sil_Sort_ID(void);
int uzlaznoSortiranjeID(const void*, const void*);
int silaznoSortiranjeID(const void*, const void*);

//brisanje
void brisanje_Podataka_Izbornik(void);
void brisanjeID(void);
void brisanjeIme(void);
void brisanjeCijena(void);
void brisanjeKolicina(void);

void zavrsetak_Programa(void);

#endif // !MARTIN_VEDRIS_HEADER_USMENI